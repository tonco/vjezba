import React, { Component } from 'react';

import CategoryList from '../containers/category_list';
import CategoryDetail from '../containers/category_detail';
import AddSupplierForm from '../containers/add_supplier_form';

export default class App extends Component {
  render() {
    return (
    	<div className="app">
	      <CategoryList />
	      <AddSupplierForm/>
	      <CategoryDetail/>
	    </div>
    );
  }
}

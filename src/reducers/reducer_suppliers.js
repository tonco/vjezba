import { CATEGORY_ADDED } from '../reducers/reducer_categories'
export const SUPPLIER_ADDED = 'SUPPLIER_ADDED'
export default function(state = [], action) {
		switch(action.type){
		case CATEGORY_ADDED:
			state.push({category : action.data, suppliers : []});
			return state;
		case SUPPLIER_ADDED:
			state.forEach(function(entry) {
    			if (entry.category == action.data.category){
    				entry.suppliers.push(action.data.supplier);
    			}
			});
		}
		return state.slice(); // klonira array
}
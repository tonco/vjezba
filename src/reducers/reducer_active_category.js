export const CATEGORY_SELECTED = 'CATEGORY_SELECTED'
export const SHOW_ALL = 'SHOW_ALL'

export default function(state = null, action){
	switch(action.type){
		case CATEGORY_SELECTED:
			return action.data;
		case SHOW_ALL:
			return action.data;
	}

	return state;
}
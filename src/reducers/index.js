import { combineReducers } from 'redux';
import CategoriesReducer from './reducer_categories';
import ActiveCategory from './reducer_active_category';
import SupliersReducer from './reducer_suppliers';

const rootReducer = combineReducers({
  categories : CategoriesReducer,
  ActiveCategory : ActiveCategory,
  suppliers : SupliersReducer
});

export default rootReducer;

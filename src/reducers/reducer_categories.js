export const CATEGORY_ADDED = 'CATEGORY_ADDED'
export default function(state = [], action) {
		switch(action.type){
		case CATEGORY_ADDED:
			var flag = 0;
			state.forEach(function(entry) {
				if (entry == action.data){
					//već postoji kategorija sa tim imenom!
					flag = 1;
					return state;
				}
			});
			if (flag == 0){
				state.push(action.data);
				//return [action.data, ...state];
			}
		}
		return state.slice();
	}
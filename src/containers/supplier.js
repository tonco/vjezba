import React, { Component } from 'react';


export default class Supplier extends Component{

	constructor (props){
		super(props);
		var toggle = false;
		this.toggleExpand = this.toggleExpand.bind(this);

	}

	toggleExpand(event){
			event.target.innerHTML = "";
			event.target.style.height = "";

			var name = this.props.supplier.name;
			var email = this.props.supplier.email;
			var city = this.props.supplier.city;
			var mob = this.props.supplier.mob;
			var web = this.props.supplier.web;
			var adress = this.props.supplier.adress;
			var phone = this.props.supplier.phone;
			var country = this.props.supplier.country;

			this.toggle = !this.toggle;
			if (this.toggle == true){
				event.target.style.height = "100px";
				event.target.innerHTML = "name : "+name +"&emsp;&emsp;&emsp;      email : "+email+"&emsp;&emsp;&emsp;      city : "+city+
				"</br>mob : "+mob+"&emsp;&emsp;&emsp; phone : "+phone+"&emsp;&emsp;&emsp; web : "+web+
				"</br>adress : "+adress+"&emsp;&emsp;&emsp; country : "+country;
			}else{
				event.target.style.height = "50px";
				event.target.innerHTML = "name : "+name;
			}	
	}
			
	fillSupplier(){
		var name = this.props.supplier.name;
		return "name : "+name;
	}

	render(){
		return(
				<div className="supplier" 
				onClick={this.toggleExpand}
				key={this.props.supplier.name}>
				{this.fillSupplier()}
				</div>
			);
	}


}
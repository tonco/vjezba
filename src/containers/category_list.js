import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectCategory, showAllCategories } from '../actions/index';
import { bindActionCreators } from 'redux';
import AddCategoryForm from './add_category_form';



class CategoryList extends Component{

	renderShowAll(){
		if (this.props.categories.length > 1){
			return(
				<div 
					onMouseOver={this.HoverStart}
					onMouseOut={this.HoverEnd}
					onClick={() => this.props.showAllCategories(this.props.categories)}>
					Show All
				</div>
				);
		}
	}

	renderList(){
		return this.props.categories.map((cat) => {
			return(
				<div key={cat}
					onMouseOver={this.HoverStart}
					onMouseOut={this.HoverEnd}
					onClick={() => this.props.selectCategory(cat)}>{cat}</div>
				);
		});
	}
	render(){
		return (
			<div className="category_list" >
					<div className="list_header">Categories</div>
					{this.renderShowAll()}
					{this.renderList()}
				<AddCategoryForm/>
			</div>

			);
	}

	HoverStart(event){
		event.target.style.background= '#666666';
	}

	HoverEnd(event){
		event.target.style.background= '#7C7C7C';
	}

}


function mapStateToProps(state){
	return {
		categories : state.categories
	};
}

function mapDispatchToProps(dispatch){
	//whenever SelectBook is called, the result should be passed
	// to all of our reducers
	return bindActionCreators({ selectCategory, showAllCategories }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(CategoryList);
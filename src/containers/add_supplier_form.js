import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addSupplier } from '../actions/index';

class AddSupplierForm extends Component{
	constructor(props){
		super(props);

		this.state = { name: '', email: '',
					  phone: '', mob: '', web: '',
					  adress : '', postal: '', city: '', country: ''};
		this.onFormSubmit = this.onFormSubmit.bind(this);
		this.onInputChange = this.onInputChange.bind(this);


	}

	onFormSubmit(event){
		event.preventDefault();
		this.props.addSupplier({ name : this.state.name, city : this.state.city, email: this.state.email,
								 mob : this.state.mob, web : this.state.web, adress : this.state.adress,
								 postal : this.state.postal, city: this.state.city, country: this.state.country}, this.props.category);
		this.setState( { name : '', city : '', email : '', mob: '', web: '', adress: '', postal: '', city:'', country: ''});
	}

	onInputChange(event){
		this.setState( { [event.target.placeholder] : event.target.value});
	}

	render(){
		return(
			<div>
				<form className="add_supplier_form" onSubmit={this.onFormSubmit}>
					<input
						placeholder="name"
						value={this.state.name}
						onChange={this.onInputChange}
					/><br></br>
					<input
						placeholder="email"
						value={this.state.email}
						onChange={this.onInputChange}
					/>
					<input
						style={{width:60+'px'}}
						placeholder="phone"
						value={this.state.phone}
						onChange={this.onInputChange}
					/>
					<input
						style={{width:60+'px'}}
						placeholder="mob"
						value={this.state.mob}
						onChange={this.onInputChange}
					/>
					<input
						placeholder="web"
						value={this.state.web}
						onChange={this.onInputChange}
					/><br></br>
					<input
						placeholder="adress"
						value={this.state.adress}
						onChange={this.onInputChange}
					/>
					<input
						style={{width:60+'px'}}
						placeholder="postal"
						value={this.state.postal}
						onChange={this.onInputChange}
					/>
					<input
						style={{width:60+'px'}}
						placeholder="city"
						value={this.state.city}
						onChange={this.onInputChange}
					/>
					<input
						placeholder="country"
						value={this.state.country}
						onChange={this.onInputChange}
					/>
					<button type="submit"/>
				</form>
			</div>
			);
	}
}


function mapDispatchToProps(dispatch){
	return bindActionCreators({ addSupplier }, dispatch);
}


function mapStateToProps(state){
	return{
		category : state.ActiveCategory,
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(AddSupplierForm);
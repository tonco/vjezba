import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addCategory } from '../actions/index';

class AddCategoryForm extends Component{
	constructor(props){
		super(props);

		this.state = { title: ''};

		this.onFormSubmit = this.onFormSubmit.bind(this);
		this.onInputChange = this.onInputChange.bind(this);

	}

	onFormSubmit(event){
		event.preventDefault();
		if (this.state.title == ''){
			alert('title is empty!');
		}else{
			this.props.addCategory(this.state.title);
			this.setState( { title : ''});
		}
	}

	onInputChange(event){
		this.setState( { title: event.target.value});
	}

	render(){
		return(
				<form className="new_category" onSubmit={this.onFormSubmit}>
					<input
						placeholder="add a category"
						value={this.state.title}
						onChange={this.onInputChange}
					/>
					<button type="submit"/>
				</form>
			);
	}
}


function mapDispatchToProps(dispatch){
	return bindActionCreators({ addCategory }, dispatch);
}

export default connect(null, mapDispatchToProps)(AddCategoryForm);
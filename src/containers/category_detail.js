import React, { Component } from 'react';
import { connect } from 'react-redux';
import AddSupplierForm from './add_supplier_form';
import Supplier from './supplier';

class CategoryDetail extends Component{

	constructor (props){
		super(props);
		var suppliers = [];
		var category = [];
		this.changeSuppliers = this.changeSuppliers.bind(this);
		this.showAll = this.showAll.bind(this);
		//this.ShowSuppliers = this.ShowSuppliers.bind(this);
	}
	
	showAll(){
		var that = this;
		return this.category.map((entry) => {
			return(
					<div className="category">
						<div className="category_title" key={entry}
						>
						{entry}	 
						</div>
						<div>{that.renderSuppliers(entry)}</div>
					</div>
				);
				
		});
	}

	



	renderSuppliers(category){
		this.changeSuppliers(category);
		return this.suppliers.map((supplier) => {
			return(
				<Supplier supplier={supplier}/>
				);
		});
	}
	changeSuppliers(category){
		var that= this;
		this.props.suppliers.forEach(function(entry) {
	    		if (entry.category == category){
	    			that.suppliers = entry.suppliers;
	    		}
			});
	}

	


	render(){
		this.category = this.props.category;
		if (!this.category){
			return <div className="category_detail">Please select a category</div>;
		}else if ( this.category.length > 1  && this.category.constructor === Array){
			return (
				<div className="category_detail" >
					{this.showAll()}
				</div>
				);
		}

		return (
			<div className="category_detail">
				<div className="category_title">{this.category}</div>
				<div>{this.renderSuppliers(this.category)}</div>
			</div>
			);
	}
}





function mapStateToProps(state){
	return{
		category : state.ActiveCategory,
		suppliers : state.suppliers,
		categories : state.categories
	};
}

export default connect(mapStateToProps)(CategoryDetail);
import { CATEGORY_SELECTED, SHOW_ALL } from '../reducers/reducer_active_category'
import { CATEGORY_ADDED } from '../reducers/reducer_categories'
import { SUPPLIER_ADDED } from '../reducers/reducer_suppliers'


function internSelectCat(category){
	return {
		type : CATEGORY_SELECTED,
		data : category
	};
}


export function selectCategory(category){
	// selectCat is an ActionCreator, it needs to return an action,
	// - an object with a type property.
	return dispatch => { 
        dispatch(internSelectCat(category));
    }
}

function internShowAll(data) {
    return{
        type : SHOW_ALL,
        data: data
    }
}

export function showAllCategories(categories){
    return dispatch => { 
        dispatch(internShowAll(categories));
    }
}

function internAddCategory(data){
	return{
		type : CATEGORY_ADDED,
		data : data
	}
}

export function addCategory(new_title){

	return dispatch => { 
        dispatch(internAddCategory(new_title));
    }
}


function internAddSupplier(supplier, category){
	return{
		type: SUPPLIER_ADDED,
		data : {supplier : supplier, category : category}
	}
}

export function addSupplier(supplier, category){

	return dispatch => { 
        dispatch(internAddSupplier(supplier, category));
    }
}
/*export function selectCategory(category){
	// selectCat is an ActionCreator, it needs to return an action,
	// - an object with a type property.
	return {
		type : 'CATEGORY_SELECTED',
		data : category
	};
}

export function showAllCategories(categories){
	return{
		type : 'SHOW_ALL',
		data : categories
	}
}

export function addCategory(new_title){

	return{
		type : 'CATEGORY_ADDED',
		data : { title : new_title }
	}
}

export function addSupplier(supplier, category){

	return{
		type: 'SUPPLIER_ADDED',
		data : {supplier : supplier, category : category}
	}
}*/